import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class SendDataService {
  token: string = null;
  urls = {
    usuarios: 'https://gorest.co.in/public-api/users?_format=json&access-token=',
    Nuevousuarios: 'https://gorest.co.in/public-api/users',
    Editarusuarios: 'https://gorest.co.in/public-api/users?_format=json&access-token=',
  };
  usuarios = undefined;
  header: HttpHeaders;
  sms: string = null;
  usuario_m = {
    "first_name": null,
    "last_name": null,
    "email": null,
    "status": "active"
  };

  constructor(
    private http: HttpClient,) {
    this.header = new HttpHeaders();
    this.header.set('Content-Type', 'application/json');
    this.header.set('X-Requested-Withn', 'XMLHttpRequest');
    this.header.set('Access-Control-Allow-Origin', '*');
    this.token = 'm5GSNpDry-a_g4gI2JC62TPFUOC_qSDF4FE3';


  }

  editUsuario(id, usuario) {
    this.usuario_m = usuario;

    this.header = new HttpHeaders({
      Authorization: 'Bearer ' + this.token,
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'X-Requested-With': 'XMLHttpRequest'
    });
    const options = {headers: this.header};
    let url = this.urls.Nuevousuarios;
    if (id != 0) {
      url = url + "/" + id;
    }

    this.sms = null;

    return this.http.patch(url, this.usuario_m, options)

      .subscribe(
        (response) => {
          const resSTR = JSON.stringify(response);
          const resJSON = JSON.parse(resSTR);
          console.error('setUsuario');
          console.error(resJSON);
          this.ConsultarUsuarios();
        },
        (error) => {
          const resSTR = JSON.stringify(error);
          const resJSON = JSON.parse(resSTR);
          this.sms = resJSON.message;
          if (resJSON.result !== undefined) {
            this.sms = resJSON.result[0].message
          }
          console.error(error);
        }
      );

  }

  crearUsuario(id, usuario) {
    this.usuario_m = usuario;
    if (this.usuario_m.email == null) {
      this.sms = 'Correo vacio';
      return null
    }
    ;
    let n_u = {
      "first_name": this.usuario_m.first_name,
      "last_name": this.usuario_m.last_name,
      "email": this.usuario_m.email,
      "gender": 'male',
      "status": "active",
      dob: null,
      phone: null,
      website: null,
      address: null

    }

    this.header = new HttpHeaders({
      Authorization: 'Bearer ' + this.token,
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'X-Requested-With': 'XMLHttpRequest'
    });
    const options = {headers: this.header};
    let url = this.urls.Nuevousuarios;
    if (id != 0) {
      url = url + "/" + id;
    }

    this.sms = null;

    return this.http.post(url, n_u, options)

      .subscribe(
        (response) => {
          const resSTR = JSON.stringify(response);
          const resJSON = JSON.parse(resSTR);
          console.error('setUsuario');
          console.error(resJSON);
          this.ConsultarUsuarios();
        },
        (error) => {
          const resSTR = JSON.stringify(error);
          const resJSON = JSON.parse(resSTR);
          this.sms = resJSON.message;
          if (resJSON.result !== undefined) {
            this.sms = resJSON.result[0].message
          }
          console.error(error);
        }
      );

  }

  borrarUsuario(id) {

    this.header = new HttpHeaders({
      Authorization: 'Bearer ' + this.token,
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'X-Requested-With': 'XMLHttpRequest'
    });
    let url = this.urls.Nuevousuarios;
    if (id != 0) {
      url = url + "/" + id;
    }

    this.sms = null;
    this.header = new HttpHeaders({
      Authorization: 'Bearer ' + this.token,
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'X-Requested-With': 'XMLHttpRequest'
    });
    const options = {headers: this.header};

    return this.http.delete(url, options)

      .subscribe(
        (response) => {
          const resSTR = JSON.stringify(response);
          const resJSON = JSON.parse(resSTR);
          console.error('setUsuario');
          console.error(resJSON);
          this.ConsultarUsuarios();
        },
        (error) => {
          const resSTR = JSON.stringify(error);
          const resJSON = JSON.parse(resSTR);
          this.sms = resJSON.message;
          if (resJSON.result !== undefined) {
            this.sms = resJSON.result[0].message
          }
          console.error(error);
        }
      );

  }


  getError() {
    return this.sms;
  }

  setToken(val) {
    this.token = val;

  }

  getUser() {
    return this.usuarios;
  }

  ConsultarUsuarios() {

    const options = {headers: this.header};
    this.sms = null;
    this.http.get(this.urls.usuarios + this.token, options)

      .subscribe(
        (response) => {

          const resSTR = JSON.stringify(response);
          const resJSON = JSON.parse(resSTR);
          this.usuarios = resJSON.result;
          //return resJSON.result;

        },
        (error) => {
          const resSTR = JSON.stringify(error);
          const resJSON = JSON.parse(resSTR);
          this.sms = resJSON.message;

          //return [{}];
        }
      );
  }
}
