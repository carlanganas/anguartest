import {Component, OnInit} from '@angular/core';
import {SendDataService} from "../send-data.service";

@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.scss']
})
export class TablaComponent implements OnInit {
  editar = 0;

  usuarios_header = {
    "id": "Id",
    "first_name": "Nombre",
    "last_name": "Apellido",
    "gender": "Genero",
    "dob": "Fecha de nacimiento",
    "email": "Correo",
    "phone": "Telefono",

    "address": "Direccion",
    "status": "active",
  };


  constructor(private sendData: SendDataService) {
  }

  message = '';
  user =

    {
      "id": 0,
      "first_name": null,
      "last_name": null,
      "email": null,
      "status": "active"
    };

  Mostrar(id, elemento) {
    this.editar = 1;
    this.user = elemento

    console.dir(id);
  }

  ngOnInit() {

  }

  setNombre(event) {
    this.user.first_name = event.target.value
  }

  setApellido(event) {
    this.user.last_name = event.target.value
  }

  setEmail(event) {
    this.user.email = event.target.value
  }

  showNuevo() {
    this.editar = 2;
  }

  Editar() {
    this.sendData.editUsuario(this.user.id, this.user);
    if (this.sendData.sms == null) {
      this.editar = 0;
    }

  }

  nuevo() {
    this.sendData.crearUsuario(0, this.user);
    if (this.sendData.sms == null) {
      this.editar = 0;
    }
  }

  borrar() {
    this.sendData.borrarUsuario(this.user.id);
    if (this.sendData.sms == null) {
      this.editar = 0;
    }
  }

  regresar() {
    this.editar = 0;
  }
}
