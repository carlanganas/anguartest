import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {DataTablesModule} from 'angular-datatables';
import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {SendDataService} from "./send-data.service";
import {TablaComponent} from './tabla/tabla.component';


@NgModule({
  declarations: [
    AppComponent,
    TablaComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

    HttpClientModule,
    AppRoutingModule,
    DataTablesModule
  ],
  providers: [
    SendDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
