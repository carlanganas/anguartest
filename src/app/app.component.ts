import {Component} from '@angular/core';
import {SendDataService} from "./send-data.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'CrudTest';
  token = '';

  constructor(private sendData: SendDataService) {
    sendData.ConsultarUsuarios();

  }


  setToken(event) {
    this.token = event.target.value;
    this.sendData.setToken(this.token);
    this.Consulta();

  }

  Consulta() {
    this.sendData.ConsultarUsuarios();
    console.error('Cantidad de usuarios en total ' + this.sendData.usuarios.length);
  }
}
